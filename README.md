# rotated_img

将图像从1度旋转到180度。

![](./python/_images/featured.png)

# 积木

![](./python/_images/block.png)

# 程序实例


![](./python/_images/explame.png)


# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|python|备注|
|-----|-----|-----|-----|:-----:|-----|
|uno|||||||
|micro:bit|||||||
|mpython|||||||
|arduinonano|||||||
|leonardo|||||||
|mega2560|||||||
|行空板||||√|||

# 更新日志

V0.0.1 基础功能完成

