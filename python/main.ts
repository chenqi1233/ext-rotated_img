//% color="#1296db" iconWidth=50 iconHeight=40
namespace picture_rotate{
    //% block="加载图片[PIC]" blockType="command"
    //% PIC.shadow="string" PIC.defl="picture.png"
    export function base64_Iint(parameter: any, block: any) {
        let data=parameter.PIC.code
        Generator.addImport(`from PIL import Image`)
        Generator.addCode(`im_src = Image.open(${data})`)
     

}
    //% block="将旋转[RO]角度" blockType="command"
    //% RO.shadow="number" RO.defl="90"
    export function base64_Valve(parameter: any, block: any) {
        let data=parameter.RO.code
        Generator.addCode(`im_src.rotate(${data})`)
    } 


}
